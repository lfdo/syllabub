# Syllabub project bolt-on

Keep Python project conventions up-to-date and worry less about trifling
matters. 🍰

## Status

Unstable prototype. Please wear goggles.

## Background

High-quality software projects demand more contribution effort. Syllabub makes
assumptions so that:

- maintainers make fewer, less frequent project-level decisions
- contributors require less up-front learning, but can dive into detail when
    required
- Syllabub is not required as a dependency

## Alternatives

Other Python
[scaffolding tools](<https://en.wikipedia.org/wiki/Scaffold_(programming)>)
include:

- [Cookiecutter](https://www.cookiecutter.io)
- [Copier](https://copier.readthedocs.io)
- [PyScaffold](https://pyscaffold.org)

These tools are general-purpose, allowing a maintainer to define an entire
project template. Syllabub has fewer options and uses a built-in template.

## Usage

### First use

- Run `pdm init` to create and populate `pyproject.toml`.
- Invoke `syllabub` to generate boilerplate files.
- Run `pdm lock --dev` to generate lock file.
- Run `pdm run reuse download --all` to get license files.
- Run `make develop` to generate environment.
- Run `syllabub` again.
- Run `git init` and `git commit` initialise repository.

### Subsequent usage

- Update syllabub and/or `pyproject.toml`.
- Invoke `syllabub` to overwrite boilerplate files.
- Review and modify diff.
- Commit changes.

## Additional options

- To install local development version of a project: `pipx install --editable .` (note the "." character)

## Use case scenarios

```gherkin
Feature: Syllabub Project Generation

  Scenario: Create a new Syllabub-style project

    Given a valid pyproject.toml file
    And an empty folder
    When Syllabub is executed
    Then pyproject.toml will be updated
    And all project files will be created
    And code and placeholders will be generated

  Scenario: Update an existing Syllabub-style project

    Given a project with a valid pyproject.toml file
    When Syllabub is executed
    Then pyproject.toml will be updated
    And all project files will be overwritten
    But existing entry points will not be overwritten
    And changes must be reviewed before committing

  Scenario: Migrate an existing project to Syllabub conventions

    Given a project with a valid pyproject.toml file
    When Syllabub is executed
    Then pyproject.toml will be updated
    And all project files will be overwritten
    But existing entry points will not be overwritten
    And non-syllabub project files must be removed
    And changes must be reviewed before committing
```

<!-- start @generated footer -->

# Development environment

## Install prerequisites

- Python 3.10
- pdm
- make

## Instructions

- Fork the upstream repository.
- `git clone [fork-url]`
- `cd [project-folder]`
- Run `make develop` to initialise your development environment.

You can use any text editor or IDE that supports virtualenv / pdm. See the
Makefile for toolchain details.

Please `make test` and `make lint` before submitting changes.

## Make targets

```
USAGE: make [target]

help    : Show this message.
develop : Set up Python development environment.
run     : Run from source.
clean   : Remove all build artefacts.
test    : Run tests and generate coverage report.
lint    : Fix or warn about linting errors.
build   : Clean, test, lint, then generate new build artefacts.
publish : Upload build artefacts to PyPI.
```

# Sharing and contributions

```
Syllabub project bolt-on
https://lofidevops.neocities.org
Copyright 2022 David Seaward and contributors
SPDX-License-Identifier: AGPL-3.0-or-later
```

Shared under AGPL-3.0-or-later. We adhere to the Contributor Covenant 2.1, and
certify origin per DCO 1.1 with a signed-off-by line. Contributions under the
same terms are welcome.

Submit security and conduct issues as private tickets. Sign commits with
`git commit --signoff`. For a software bill of materials run `reuse spdx`. For
more details see CONDUCT, COPYING and CONTRIBUTING.
