<!--
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2022 David Seaward and contributors
-->

# Syllabub roadmap (very draughty)

## Generated

Add @generated headers, see https://generated.at

## Extras

- Replace make with pdm scripts: https://pdm.fming.dev/latest/usage/scripts/
- Add Gherkin, Mermaid, FAQ?

## Module

Check for `module.py` or `module/__init__.py` or `src/module/__init__.py`.

If missing, create `src/module/???.py` with "Hello world" contents.

## Project files

Detect and mention DRAGONS, GOVERNANCE (but do not create if missing)

## pdm hooks

Switch from Makefile to PDM scripts:

https://pdm.fming.dev/latest/usage/scripts/

## Skipping tasks

Perform all tasks by default. Optionally set explicit tasks:

```
[syllabub]
tasks = ["readme", "makefile"]
```

Tasks:

- readme (header and footer)
- project-files
- python-module
- makefile

Note:

- If tasks is not set, empty or \["all"\] then do all.
- If tasks is \["skip-all"\] then do none.

## Proposals

- make targets = "all" or "build,x,y"
- build == build-tarball-and-wheel
- or add build-tarball-only, build-wheel-only
- build-snap: Update existing snap/snapcraft.yaml
- build-zipapp: https://docs.python.org/3/library/zipapp.html
- Confirm suitability for Debian packaging (lint?)

---

# Draft documentation

## Background

As the maintainer of a free software project, you presumably want to:

- set contributor expectations
- enable packaging and redistribution
- enable commercial adoption

Syllabub aims to streamline the decisions you need to make, and express the
results in human- and machine-readable conventions.

Syllabub does not aim to encompass all possible choices and permutations. If
your policies are very different, Syllabub may not be for you.

## Implementation

Syllabub removes all files from your root project folder and populates it with
well-known replacements.

The following filetypes are preserved by default: lock, md, png, toml

The following files are modified in-place: .gitignore, README.md

## Maintainer decisions

These are the decisions that need to be made, and the choices available in
Syllabub.

### Project name (and title)

Every project needs a name. A lowercase ASCII name with no/limited punctuation
translates well across contexts and platforms.

You may also want to define a separate project "title" that includes
punctuation and/or special characters, as used in freeform writing.

### Summary

A short, one-line summary of your project. How pithy or detailed is up to you.

### Primary URL

A landing page for interested contributors and/or users. The website hosting
the project code is a reasonable choice.

### Primary license

It's easiest to use the license commonly recommended by your development
community. Otherwise, use a license picker that you trust.

See https://spdx.org/licenses/ for all possible options.

### Primary copyright

The copyright byline for your project goes hand-in-hand with the license. This
is freeform legal text. A useful, low-maintenance format is:

```
Copyright <YYYY> <Maintainer Name> and contributors
```

or:

```
Copyright <YYYY> <Project Title> authors
```

You do not need to update the year.

### Origin sign-off

Contributors need to demonstrate they are permitted to make contributions (for
example, they make the contribution in the scope of their work contract). The
`git commit --signoff` command provides a low-effort way to do this.

For some security projects, you may require GPG signatures to further guarantee
identity and \[correctness\]. Options:

- `DCO-1.1-git-signoff`
- `DCO-1.1-gpg-signoff`

### Code of conduct

As a project maintainer you will be expected to resolve conduct problems in
project areas. A code of conduct gives a clear signal that anti-social
behaviour is not welcome, making it less likely. Options:

- Contributor Covenant
- Ubuntu Code of Conduct

---

## Syllabub conventions

This is under development.

---

# Syllabub conventions

Syllabub relies on the following conventions. They are non-configurable and
hopefully not onerous.

## pyproject.toml

Wherever possible, standard pyproject settings are used. Where standard values
do not exist, they are not defined, they are added under the
`[tool.syllabub.project]` node.

- name: short project name (matches the module name)
- version: version number
- description: short, one-line description
- license.text: primary project license (SPDX shortcode)
- tool.syllabub.files.logo: relative path to logo file
- tool.syllabub.project.title: full project name (if different to short name)
- tool.syllabub.project.conduct: code of conduct (shortcode)
- tool.syllabub.project.copyright: primary copyright line
- tool.syllabub.project.origin: origin text (shortcode)
- tool.syllabub.project.sbom: invocation for a software bill of materials
- tool.syllabub.project.url: primary project url

## License shortcodes

See https://spdx.org/licenses/

## Conduct shortcodes

`contributor-covenant-2.1`

The latest recorded version of https://www.contributor-covenant.org

`ubuntu-code-of-conduct-2.0`

The latest recorded version of https://ubuntu.com/community/code-of-conduct

## Origin shortcodes

Both refer to the latest recorded version of https://developercertificate.org

`DCO-1.1-git-signoff`

Contributors use `git --signoff`. Sign-off text appears in the commit.

`DCO-1.1-gpg-signoff`

Contributors use `git --gpg-sign`. Commit has a cryptographic signature.

## Makefile

Use `## comments`

```
     help :  Show this message.
     lint :  Enforce known style rules.
     test :  Run tests and generate coverage report.

Always lint and test before submitting a contribution.

     sbom :  Generate a software bill of materials.
    wheel :  Build a Python wheel.
     pypi :  Publish wheel to PyPI.
     snap :  Build a snapfile.
snapcraft :  Publish snapfile to snapcraft.io.

For more specific tasks, use the underlying tools, such as
pdm, black or reuse. See the Makefile for hints.
```
